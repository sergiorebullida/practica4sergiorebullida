package clases;

import java.util.Scanner;

public class Filmografia {
	// atributos
	private Pelicula[] peliculas;

	// constructores
	/**
	 * Constructor para crear un vector que contenga peliculas
	 */
	public Filmografia() {
		this.peliculas = new Pelicula[5];
	}

	// metodo alta
	/**
	 * Metodo que da de alta peliculas
	 * 
	 * @param titulo       nombre de la pelicula
	 * @param genero       genero cinematografico
	 * @param calificacion puntuacion de la pelicula
	 * @param annoEstreno  a�o en el que se estreno la pelicula
	 * @param duracion     tiempo de duracion de la pelicula, en minutos
	 * @param productora   nombre de la productora de la pelicula
	 * @param actor1       nombre del primer actor de la pelicula
	 * @param actor2       nombre del segundo actor de la pelicula
	 */
	public void altaPelicula(String titulo, String genero, double calificacion, int annoEstreno, int duracion,
			String productora, Actor actor1, Actor actor2) {
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] == null) {
				peliculas[i] = new Pelicula(titulo, genero, calificacion, annoEstreno, duracion, productora, actor1,
						actor2);
				break;
			}
		}
	}

	// metodo buscar
	/**
	 * Metodo que busca un actor
	 * 
	 * @param nombreIntroducido nombre del actor introducido por el usuario
	 * @return datos del actor introducido
	 */
	public Actor buscarActor(String nombreIntroducido) {
		boolean existir = false;
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				if (peliculas[i].getActor1().getNombre().equalsIgnoreCase(nombreIntroducido)) {
					System.out.println(peliculas[i].getActor1());
					existir = true;
				} else if (peliculas[i].getActor2().getNombre().equalsIgnoreCase(nombreIntroducido)) {
					System.out.println(peliculas[i].getActor2());
					existir = true;
				}
			}
		}

		if (existir == false) {
			System.out.println("Sin informacion");
		}
		return null;
	}

	/**
	 * Metodo que busca una pelicula
	 * 
	 * @param tituloIntroducido titulo de la pelicula introducida por el usuario
	 * @return datos de la pelicula introducida
	 */
	public Pelicula buscarPelicula(String tituloIntroducido) {
		boolean existir = false;
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				if (peliculas[i].getTitulo().equalsIgnoreCase(tituloIntroducido)) {
					System.out.println(peliculas[i]);
					existir = true;
				}
			}
		}

		if (existir == false) {
			System.out.println("Sin informacion");
		}
		return null;
	}

	// metodo eliminar
	/**
	 * Metodo para eliminar una pelicula
	 * 
	 * @param tituloIntroducido titulo de la pelicula que se quiere eliminar
	 */
	public void eliminarPelicula(String tituloIntroducido) {
		boolean existir = false;
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				if (peliculas[i].getTitulo().equalsIgnoreCase(tituloIntroducido)) {
					peliculas[i] = null;
					existir = true;
				}
			}
		}

		if (existir == true) {
			listarPeliculas();
		} else {
			System.out.println("No existe la pelicula introducida");
		}
	}

	// metodo listar
	/**
	 * Metodo para listar todos los actores
	 */
	public void listarActores() {
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				System.out.println(peliculas[i].getActor1());
				System.out.println(peliculas[i].getActor2());
			}
		}
	}

	/**
	 * Metodo para listar todas las peliculas
	 */
	public void listarPeliculas() {
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				Pelicula.mostrarPeli(peliculas[i]);
			}
		}
	}

	Scanner input2 = new Scanner(System.in);

	// metodo cambiar
	/**
	 * Metodo que cambia todos los atributos de un actor
	 * 
	 * @param nombreIntroducido nombre del actor que se modificar�
	 */
	public void cambiarActor(String nombreIntroducido) {
		boolean existir = false;
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				if (peliculas[i].getActor1().getNombre().equalsIgnoreCase(nombreIntroducido)) {
					System.out.println("Introduce un nuevo nombre:");
					String nuevoNombre = input2.nextLine();
					peliculas[i].getActor1().setNombre(nuevoNombre);

					System.out.println("Introduce los apellidos:");
					String nuevosApellidos = input2.nextLine();
					peliculas[i].getActor1().setApellidos(nuevosApellidos);

					System.out.println("Introduce la edad:");
					int nuevaEdad = input2.nextInt();
					peliculas[i].getActor1().setEdad(nuevaEdad);
					input2.nextLine();

					System.out.println("Introduce su nacionalidad:");
					String nuevaNacionalidad = input2.nextLine();
					peliculas[i].getActor1().setNacionalidad(nuevaNacionalidad);

					existir = true;
				} else if (peliculas[i].getActor2().getNombre().equalsIgnoreCase(nombreIntroducido)) {
					System.out.println("Introduce un nuevo nombre:");
					String nuevoNombre = input2.nextLine();
					peliculas[i].getActor2().setNombre(nuevoNombre);

					System.out.println("Introduce los apellidos:");
					String nuevosApellidos = input2.nextLine();
					peliculas[i].getActor2().setApellidos(nuevosApellidos);

					System.out.println("Introduce la edad:");
					int nuevaEdad = input2.nextInt();
					peliculas[i].getActor2().setEdad(nuevaEdad);
					input2.nextLine();

					System.out.println("Introduce su nacionalidad:");
					String nuevaNacionalidad = input2.nextLine();
					peliculas[i].getActor2().setNacionalidad(nuevaNacionalidad);
					existir = true;
				}
			}
		}

		if (existir == true) {
			listarActores();
		} else {
			System.out.println("No existe el actor introducido");
		}
	}

	/**
	 * Metodo que cambia todos los atributos de la pelicula que se busque
	 * 
	 * @param tituloIntroducido titulo de la pelicula, a la que cambiar sus
	 *                          atributos
	 */
	public void cambiarPelicula(String tituloIntroducido) {
		boolean existir = false;
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				if (peliculas[i].getTitulo().equalsIgnoreCase(tituloIntroducido)) {
					System.out.println("Introduce un nuevo titulo:");
					String nuevoTitulo = input2.nextLine();
					peliculas[i].setTitulo(nuevoTitulo);

					System.out.println("Introduce el genero cinematogr�fico:");
					String nuevoGenero = input2.nextLine();
					peliculas[i].setGenero(nuevoGenero);

					System.out.println("Introduce su calificacion:");
					double nuevaCalificacion = input2.nextDouble();
					peliculas[i].setCalificacion(nuevaCalificacion);

					System.out.println("Introduce el a�o de estreno:");
					int nuevoAnno = input2.nextInt();
					peliculas[i].setAnnoEstreno(nuevoAnno);

					System.out.println("Introduce la duracion:");
					int nuevaDuracion = input2.nextInt();
					peliculas[i].setDuracion(nuevaDuracion);
					input2.nextLine();

					System.out.println("Introduce la productora de la pelicula:");
					String nuevaProductora = input2.nextLine();
					peliculas[i].setProductora(nuevaProductora);

					existir = true;
				}
			}
		}

		if (existir == true) {
			listarPeliculas();
		} else {
			System.out.println("No existe la pelicula introducida");
		}
	}

	// metodo listar con condicion
	/**
	 * Metodo que lista los actores a partir de su nacionalidad
	 * 
	 * @param nacionalidadIntroducida nacionalidad introducida por el usuario
	 */
	public void listarActorPorNacionalidad(String nacionalidadIntroducida) {
		boolean existir = false;
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				if (peliculas[i].getActor1().getNacionalidad().equalsIgnoreCase(nacionalidadIntroducida)) {
					System.out.println(peliculas[i].getActor1());
					existir = true;
				} else if (peliculas[i].getActor2().getNacionalidad().equalsIgnoreCase(nacionalidadIntroducida)) {
					System.out.println(peliculas[i].getActor2());
					existir = true;
				}
			}
		}

		if (existir == false) {
			System.out.println("Nacionalidad incorrecta");
		}
	}

	/**
	 * Metodo que busca las peliculas producidas por una productora
	 * 
	 * @param productoraIntroducida productora introducida por teclado por el
	 *                              usuario
	 */
	public void listarPeliculaPorProductora(String productoraIntroducida) {
		boolean existir = false;
		for (int i = 0; i < peliculas.length; i++) {
			if (peliculas[i] != null) {
				if (peliculas[i].getProductora().equalsIgnoreCase(productoraIntroducida)) {
					Pelicula.mostrarPeli(peliculas[i]);
					existir = true;
				}
			}
		}

		if (existir == false) {
			System.out.println("No hay ninguna pelicula de la productora " + productoraIntroducida);
		}
	}

	// metodo ordenar
	/**
	 * Metodo para ordenar desde pelicula mas antigua a mas reciente
	 */
	public void ordenarPeliculasAnnoReciente() {
		for (int i = 0; i < peliculas.length - 1; i++) {
			if (peliculas[i] != null) {
				for (int j = i + 1; j < peliculas.length; j++) {
					if (peliculas[j] != null) {
						if (peliculas[j].getAnnoEstreno() < peliculas[i].getAnnoEstreno()) {
							Pelicula aux = peliculas[i];
							peliculas[i] = peliculas[j];
							peliculas[j] = aux;
						}
					}
				}
			}
		}
		listarPeliculas();
	}

	/**
	 * Metodo para ordenar desde pelicula mas reciente a mas antigua
	 */
	public void ordenarPeliculasAnnoAntigua() {
		for (int i = 0; i < peliculas.length - 1; i++) {
			if (peliculas[i] != null) {
				for (int j = i + 1; j < peliculas.length; j++) {
					if (peliculas[j] != null) {
						if (peliculas[j].getAnnoEstreno() > peliculas[i].getAnnoEstreno()) {
							Pelicula aux = peliculas[i];
							peliculas[i] = peliculas[j];
							peliculas[j] = aux;
						}
					}
				}
			}
		}
		listarPeliculas();
	}

}