package clases;

public class Pelicula {
	// atributos
	private String titulo;
	private String genero;
	private double calificacion;
	private int annoEstreno;
	private int duracion;
	private String productora;
	private Actor actor1;
	private Actor actor2;

	// constructor
	/**
	 * Constructor vacio
	 */
	public Pelicula() {

	}

	/**
	 * Constructor para crear una pelicula con solo dos atributos
	 * 
	 * @param titulo titulo de la pelicula
	 * @param annoEstreno a�o en el que se estreno la pelicula
	 */
	public Pelicula(String titulo, int annoEstreno) {
		this.titulo = titulo;
		this.annoEstreno = annoEstreno;
	}

	/**
	 * Constructor con todos los atributos de Pelicula
	 * 
	 * @param titulo nombre de la pelicula
	 * @param genero genero cinematografico
	 * @param calificacion valoracion sobre cinco
	 * @param annoEstreno a�o en el que se estreno la pelicula
	 * @param duracion duracion en minutos de la pelicula
	 * @param productora nombre de la compa�ia productora
	 * @param actor1 primer actor asociado
	 * @param actor2 segundo actor
	 */
	public Pelicula(String titulo, String genero, double calificacion, int annoEstreno, int duracion, String productora,
			Actor actor1, Actor actor2) {
		this.titulo = titulo;
		this.genero = genero;
		this.calificacion = calificacion;
		this.annoEstreno = annoEstreno;
		this.duracion = duracion;
		this.productora = productora;
		this.actor1 = actor1;
		this.actor2 = actor2;
	}

	// getter y setter
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	public int getAnnoEstreno() {
		return annoEstreno;
	}

	public void setAnnoEstreno(int annoEstreno) {
		this.annoEstreno = annoEstreno;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getProductora() {
		return productora;
	}

	public void setProductora(String productora) {
		this.productora = productora;
	}

	public Actor getActor1() {
		return actor1;
	}

	public void setActor1(Actor actor1) {
		this.actor1 = actor1;
	}

	public Actor getActor2() {
		return actor2;
	}

	public void setActor2(Actor actor2) {
		this.actor2 = actor2;
	}

	// toString
	@Override
	public String toString() {
		return titulo + "\nGenero: " + genero + "\nCalificacion: " + calificacion + "\nA�o: " + annoEstreno
				+ "\nDuracion: " + duracion + " min" + "\nProductora: " + productora + "\n" + actor1 + "\n" + actor2;
	}

	// metodo mostrarPeli
	/**
	 * Metodo que muestra unicamente dos atributos
	 * 
	 * @param pelicula1 pelicula que se quiere mostrar
	 */
	public static void mostrarPeli(Pelicula pelicula1) {
		System.out.println(pelicula1.titulo + ", " + pelicula1.annoEstreno);
	}

}