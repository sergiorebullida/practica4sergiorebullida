package clases;

public class Actor {
	// atributos
	private String nombre;
	private String apellidos;
	private int edad;
	private String nacionalidad;

	// constructor
	/**
	 * Constructor vacio
	 */
	public Actor() {

	}
	
	/**
	 * Constructor de Actor con todos sus atributos
	 * 
	 * @param nombre nombre del actor
	 * @param apellidos los apellidos del actor
	 * @param edad a�os del actor
	 * @param nacionalidad nacionalidad del actor
	 */
	public Actor(String nombre, String apellidos, int edad, String nacionalidad) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.nacionalidad = nacionalidad;
	}

	// getters y setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	// toString
	@Override
	public String toString() {
		return nombre + " " + apellidos + ", Edad: " + edad + ", Nacionalidad: " + nacionalidad;
	}
}