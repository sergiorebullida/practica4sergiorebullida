package programa;

import java.util.Scanner;
import clases.Filmografia;
import clases.Actor;

public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		Filmografia filmes = new Filmografia();

		Actor actor1 = new Actor("Al", "Pacino", 81, "Estadounidense");
		Actor actor2 = new Actor("Marlon", "Brando", 80, "Estadounidense");
		Actor actor3 = new Actor("Russell", "Crowe", 57, "Neozelandesa");
		Actor actor4 = new Actor("Joaquin", "Phoenix", 47, "Estadounidense");
		Actor actor5 = new Actor("Anthony", "Hopkins", 84, "Britanica");
		Actor actor6 = new Actor("Jodie", "Foster", 59, "Estadounidense");
		Actor actor7 = new Actor("Tom", "Hanks", 65, "Estadounidense");
		Actor actor8 = new Actor("Michael Clarke", "Duncan", 54, "Estadounidense");
		Actor actor9 = new Actor("Leonardo", "DiCaprio", 47, "Estadounidense");
		Actor actor10 = new Actor("Margot", "Robbie", 31, "Australiana");

		filmes.altaPelicula("El padrino", "Crimen", 4.8, 1972, 153, "Paramount Pictures", actor1, actor2);
		filmes.altaPelicula("Gladiator", "Accion", 4.6, 2000, 141, "Scott Free Productions", actor3, actor4);
		filmes.altaPelicula("El silencio de los corderos", "Terror", 4.4, 1991, 118, "Strong Heart", actor5, actor6);
		filmes.altaPelicula("La milla verde", "Drama", 4.4, 2000, 188, "Castle Rock Entertainment", actor7, actor8);
		filmes.altaPelicula("El lobo de Wall Street", "Comedia", 4.2, 2014, 180, "Paramount Pictures", actor9, actor10);
		System.out.println("Peliculas creadas correctamente");

		int opcion = 0;

		do {
			System.out.println("\n1. Buscador");
			System.out.println("2. Eliminar pelicula");
			System.out.println("3. Listar actores o peliculas");
			System.out.println("4. Cambiar nombre de actor o titulo de pelicula");
			System.out.println("5. Listar actores por nacionalidad o peliculas por productora");
			System.out.println("6. Ordenar las peliculas por fecha de estreno");
			System.out.println("7. Finalizar programa");
			System.out.println("Introduce una opcion:");
			System.out.println("----------------------");
			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {
			case 1:
				int opcionBuscar;
				do {
					System.out.println("\n1. Buscar actor");
					System.out.println("2. Buscar pelicula");
					System.out.println("3. Salir al menu principal");
					opcionBuscar = input.nextInt();
					input.nextLine();

					switch (opcionBuscar) {
					case 1:
						System.out.println("Introduce el nombre del actor a buscar:");
						String nombreActor = input.nextLine();
						filmes.buscarActor(nombreActor);
						break;

					case 2:
						System.out.println("Introduce el titulo de la pelicula:");
						String tituloPelicula = input.nextLine();
						filmes.buscarPelicula(tituloPelicula);
						break;

					case 3:
						System.out.println("SALIENDO...");
						break;

					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionBuscar != 3);
				break;

			case 2:
				System.out.println("Introduce la pelicula que quiera eliminar:");
				String tituloPelicula = input.nextLine();
				filmes.eliminarPelicula(tituloPelicula);
				break;

			case 3:
				int opcionListar;
				do {
					System.out.println("\n1. Listar actores");
					System.out.println("2. Listar peliculas");
					System.out.println("3. Salir al menu principal");
					opcionListar = input.nextInt();
					input.nextLine();

					switch (opcionListar) {
					case 1:
						filmes.listarActores();
						break;

					case 2:
						filmes.listarPeliculas();
						break;

					case 3:
						System.out.println("SALIENDO...");
						break;

					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionListar != 3);
				break;

			case 4:
				int opcionCambiar;
				do {
					System.out.println("\n1. Cambiar valores de un actor");
					System.out.println("2. Cambiar valores de una pelicula");
					System.out.println("3. Salir al menu principal");
					opcionCambiar = input.nextInt();
					input.nextLine();

					switch (opcionCambiar) {
					case 1:
						System.out.println("Introduce el actor que quiera modificar:");
						String nombreActor = input.nextLine();
						filmes.cambiarActor(nombreActor);
						break;

					case 2:
						System.out.println("Introduce la pelicula que quiera modificar:");
						String tituloModificar = input.nextLine();
						filmes.cambiarPelicula(tituloModificar);
						break;

					case 3:
						System.out.println("SALIENDO...");
						break;

					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionCambiar != 3);
				break;

			case 5:
				int opcionListarCondicion;
				do {
					System.out.println("\n1. Listar actores por nacionalidad");
					System.out.println("2. Listar peliculas por productora");
					System.out.println("3. Salir al menu principal");
					opcionListarCondicion = input.nextInt();
					input.nextLine();

					switch (opcionListarCondicion) {
					case 1:
						System.out.println("Buscar actores por nacionalidad:");
						String nacionalidadActor = input.nextLine();
						filmes.listarActorPorNacionalidad(nacionalidadActor);
						break;

					case 2:
						System.out.println("Buscar peliculas por productora:");
						String productoraPelicula = input.nextLine();
						filmes.listarPeliculaPorProductora(productoraPelicula);
						break;

					case 3:
						System.out.println("SALIENDO...");
						break;

					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionListarCondicion != 3);
				break;

			case 6:
				int opcionOrdenar;
				do {
					System.out.println("\n1. Ordenar a�o de estreno (mas reciente)");
					System.out.println("2. Ordenar a�o de estreno (mas antigua)");
					System.out.println("3. Salir al menu principal");
					opcionOrdenar = input.nextInt();
					input.nextLine();

					switch (opcionOrdenar) {
					case 1:
						System.out.println("Ordenando a mas reciente...");
						filmes.ordenarPeliculasAnnoReciente();
						break;

					case 2:
						System.out.println("Ordenando a mas antigua...");
						filmes.ordenarPeliculasAnnoAntigua();
						break;

					case 3:
						System.out.println("SALIENDO...");
						break;

					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionOrdenar != 3);
				break;

			case 7:
				System.out.println("SISTEMA APAGADO");
				break;

			default:
				System.out.println("Opcion incorrecta, vuelve a intentarlo");
				break;
			}
		} while (opcion != 7);

		input.close();
	}

}